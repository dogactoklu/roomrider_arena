#ifndef GAZEBO_PLUGINS_STAINMODELPLUGIN_HH_
#define GAZEBO_PLUGINS_STAINMODELPLUGIN_HH_

#include <ignition/math/Pose3.hh>
#include "gazebo/physics/physics.hh"
#include <gazebo/sensors/sensors.hh>
#include "gazebo/common/common.hh"
#include "gazebo/gazebo.hh"

#include <ignition/transport/Node.hh>
#include <gazebo/transport/Node.hh>

#include <thread>
#include "ros/ros.h"
#include "std_msgs/String.h"
#include "ros/callback_queue.h"
#include "ros/subscribe_options.h"

#include <math.h>

#define PI 3.14159265

namespace gazebo
{
    class GAZEBO_VISIBLE StainModelPlugin : public ModelPlugin
	{
		public: StainModelPlugin();
		public: ~StainModelPlugin();
		public: void Load(physics::ModelPtr _model, sdf::ElementPtr _sdf);

		private: void onPoseReceive(const std_msgs::StringConstPtr &msg);
		private: void onPickup();
    private: void Animate();

		private: physics::WorldPtr world;

		private: physics::ModelPtr model;
		private: std::string stain_name;
		private: ignition::math::Pose3d pose;

		private: std::unique_ptr<ros::NodeHandle> rosNode;
		private: ros::Publisher stain_collision_pub;
		private: ros::Subscriber robot_pose_subscriber;

    private: bool isPicked;
    private: double pickedupTime;
    private: event::ConnectionPtr worldConnection;
    private: physics::Link_V links;
  	};
}
#endif
